package com.devcamp.person.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.person.model.CUser;

public interface CUserRepository extends JpaRepository<CUser, Long>{
    
}
