package com.devcamp.person.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.person.model.CUser;
import com.devcamp.person.repository.CUserRepository;



@CrossOrigin
@RestController
public class UserController {
    @Autowired
    CUserRepository pCUserRepository;


    @GetMapping("/users")
    public ResponseEntity<List<CUser>> getUsersAll(){
        try {
            List<CUser> users = new ArrayList<CUser>();
            pCUserRepository.findAll().forEach(users::add);
            return new ResponseEntity<>  (users, HttpStatus.OK);

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>  (null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        
    }
    
}
